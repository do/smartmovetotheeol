import sublime, sublime_plugin

class SmartMoveToTheEolCommand(sublime_plugin.WindowCommand):
	def getLastPos(self, view, sel):
		end = sel.end()
		while ((end < view.size()) and (view.substr(end) != '\n')):
			end += 1
		return end - 1

	def run(self):
		view = self.window.active_view();
		for sel in view.sel():
			if not sel.empty():
				continue
			lastPos = self.getLastPos(view, sel)
			lastChar = view.substr(lastPos)

			curPos = sel.end()

			# insert ; and move cursor
			edit_insert = view.begin_edit()
			view.insert(edit_insert, curPos, ';')
			view.sel().clear()
			view.sel().add(sublime.Region(curPos + 1))
			view.end_edit(edit_insert)

			if (lastChar != ';' and lastChar != '{'):
				edit_insert = view.begin_edit()
				view.erase(edit_insert, sublime.Region(curPos, curPos+1))
				view.insert(edit_insert, lastPos + 1, ';')
				view.sel().clear()
				view.sel().add(sublime.Region(lastPos + 2))
				view.end_edit(edit_insert)